package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Client;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneTimeContent;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.time.OffsetDateTime;
import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StaffControllerTest extends ApiTestBase {

    @Test
    void should_return_http_status_create_and_header_location() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "/api/staffs/1"));
    }

    @Test
    void should_return_http_status_400_when_first_name_is_null() throws Exception {
        Staff staff = new Staff(null, "Hall");
        getStaffPerform(staff)
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_http_status_400_when_first_name_length_more_than_64() throws Exception {
        Staff staff = new Staff("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_http_status_200_and_staff_information_when_give_staff_id() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        MvcResult mvcResult = getStaffPerform(staff)
                .andReturn();
        mockMvc.perform(get(Objects.requireNonNull(mvcResult.getResponse().getHeader("location"))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("firstName").value("Rob"))
                .andExpect(jsonPath("lastName").value("Hall"));
    }

    @Test
    void should_return_http_status_404_when_staff_id_not_found() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff);
        mockMvc.perform(get("/api/staffs/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_empty_array_when_not_exist_staff() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(0));
    }

    @Test
    void should_return_2_staff_when_insert_two_staff() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getStaffPerform(new Staff("Tom", "Jack"));
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    void should_update_staff_when_zone_id_correct() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff);
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
    }


    @Test
    void should_return_400_when_zone_id_is_invalid() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getUpdateStaffPerform(new ZoneTimeContent("safdsfsd"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_404_when_staff_id_not_found() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        mockMvc.perform(put("/api/staffs/2/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(serialize("Asia/Chongqing")))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_staff_when_update_zone_id() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(jsonPath("firstName").value("Rob"))
                .andExpect(jsonPath("lastName").value("Hall"))
                .andExpect(jsonPath("zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_return_success_when_reservation_time_success() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1/reservations"));
    }

    @Test
    void should_return_400_when_user_name_invalid() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getClientPerform(new Client(null, "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-08-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_company_name_invalid() throws Exception {
        getStaffPerform(new Staff("Rob", "Hall"));
        getClientPerform(new Client("Sofia", null, "Africa/Nairobi", OffsetDateTime.parse("2019-08-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_reservation_success() throws Exception {
        // create
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());

        // update
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());

        // create

        MvcResult mvcResult = getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isCreated()).andReturn();


        mockMvc.perform(get(Objects.requireNonNull(mvcResult.getResponse().getHeader("Location"))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].userName").value("Sofia"))
                .andExpect(jsonPath("$[0].companyName").value("ThoughtWorks"))
                .andExpect(jsonPath("$[0].duration").value("PT1H"));
    }

    @Test
    void should_return_409_when_staff_zone_id_null() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());

        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isConflict())
                .andExpect(content().string("{firstName lastName} is not qualified."));
    }

    @Test
    void should_return_400_when_when_reservation_time_less_than_48() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());

        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-13T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isConflict())
                .andExpect(content().string("Invalid time: the application is too close from now. The interval should be greater than 48 hours.")).andReturn();
    }

    @Test
    void should_return_error_message_when_conflict_with_existing_application() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isCreated());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isConflict())
                .andExpect(content().string("The application is conflict with existing application."));
    }

    @Test
    void should_return_error_message_when_conflict_with_existing_time() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:46:00+03:00"), "PT1H"))
                .andExpect(status().isCreated());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T12:36:00+03:00"), "PT1H"))
                .andExpect(status().isConflict())
                .andExpect(content().string("The application is conflict with existing application."));
    }

    @Test
    void should_return_error_message_when_out_of_reservation_time() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T20:36:00+03:00"), "PT1H"))
                .andExpect(status().isConflict())
                .andExpect(content().string("You know, our staff has their own life."));
    }

    @Test
    void should_return_error_message_when_duration_is_invalid() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:36:00+03:00"), "PT4H"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("The application duration should be within 1-3 hours."));
    }

    @Test
    void should_return_error_message_when_duration_is_not_integer_number() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        getStaffPerform(staff)
                .andExpect(status().isCreated());
        getUpdateStaffPerform(new ZoneTimeContent("Asia/Chongqing"))
                .andExpect(status().isOk());
        getClientPerform(new Client("Sofia", "ThoughtWorks", "Africa/Nairobi", OffsetDateTime.parse("2019-10-20T11:36:00+03:00"), "PT1.5H"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("The application duration should be within 1-3 hours."));
    }

    private ResultActions getStaffPerform(Staff staff) throws Exception {
        return mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(serialize(staff)));
    }

    private ResultActions getClientPerform(Client client) throws Exception {
        return mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(serialize(client)));
    }

    private ResultActions getUpdateStaffPerform(ZoneTimeContent zoneTimeContent) throws Exception {
        return mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(serialize(zoneTimeContent)));
    }


}
