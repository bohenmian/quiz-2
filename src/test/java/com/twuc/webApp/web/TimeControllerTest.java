package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class TimeControllerTest extends ApiTestBase {

    @Test
    void should_return_all_time_zone_sort_by_zone_id() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/timezones")).andReturn();
        String[] split = mvcResult.getResponse().getContentAsString().split(",");
        assertEquals("\"Africa/Accra\"", split[1]);
    }
}
