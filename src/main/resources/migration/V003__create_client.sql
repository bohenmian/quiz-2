CREATE TABLE IF NOT EXISTS client
(
    `id`           BIGINT AUTO_INCREMENT PRIMARY KEY,
    `user_name`    VARCHAR(128) NOT NULL,
    `company_name` VARCHAR(128) NOT NULL,
    `zone_id`      VARCHAR(25)  NOT NULL,
    `start_time`    DATETIME    NOT NULL,
    `duration`     VARCHAR(25)  NOT NULL
)
