package com.twuc.webApp.exception;

public class StaffIdNotFoundException extends BaseException {

    public StaffIdNotFoundException() {
        super();
    }

    public StaffIdNotFoundException(String message) {
        super(message);
    }
}
