package com.twuc.webApp.exception;

public class OutOfReservationTimeException extends BaseException {

    public OutOfReservationTimeException() {
        super();
    }

    public OutOfReservationTimeException(String message) {
        super(message);
    }
}
