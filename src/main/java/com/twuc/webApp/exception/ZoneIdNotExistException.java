package com.twuc.webApp.exception;

public class ZoneIdNotExistException extends BaseException {

    public ZoneIdNotExistException() {
        super();
    }

    public ZoneIdNotExistException(String message) {
        super(message);
    }
}
