package com.twuc.webApp.exception;

public class NoEnoughTimeReservationException extends BaseException {

    public NoEnoughTimeReservationException() {
        super();
    }

    public NoEnoughTimeReservationException(String message) {
        super(message);
    }
}
