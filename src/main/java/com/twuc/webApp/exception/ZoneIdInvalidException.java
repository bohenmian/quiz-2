package com.twuc.webApp.exception;

public class ZoneIdInvalidException extends BaseException {
    public ZoneIdInvalidException() {
        super();
    }

    public ZoneIdInvalidException(String message) {
        super(message);
    }
}
