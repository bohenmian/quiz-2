package com.twuc.webApp.exception;

public class DurationInvalidException extends BaseException {

    public DurationInvalidException() {
        super();
    }

    public DurationInvalidException(String message) {
        super(message);
    }
}
