package com.twuc.webApp.exception;

public class TimeConflictException extends BaseException {

    public TimeConflictException() {
        super();
    }

    public TimeConflictException(String message) {
        super(message);
    }
}
