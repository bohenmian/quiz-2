package com.twuc.webApp.web;

import com.twuc.webApp.entity.Client;
import com.twuc.webApp.entity.ReservationResponse;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneTimeContent;
import com.twuc.webApp.exception.*;
import com.twuc.webApp.service.ClientService;
import com.twuc.webApp.service.StaffService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StaffController {

    private StaffService staffService;
    private ClientService clientService;

    public StaffController(StaffService staffService, ClientService clientService) {
        this.staffService = staffService;
        this.clientService = clientService;
    }


    @PostMapping("/staffs")
    public ResponseEntity createStaff(@RequestBody @Valid Staff staff) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + staffService.createStaff(staff))
                .build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity getStaff(@PathVariable Long staffId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(staffService.getStaff(staffId));
    }

    @GetMapping("/staffs")
    public ResponseEntity getAllStaff() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(staffService.getAllStaff());
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity updateStaff(@PathVariable Long staffId, @RequestBody ZoneTimeContent content) {
        staffService.updateStaff(staffId, content);
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

    @PostMapping("/staffs/{staffId}/reservations")
    public ResponseEntity orderStaff(@PathVariable Long staffId, @RequestBody @Valid Client client) {
        clientService.orderStaff(staffId, client);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + staffId + "/reservations")
                .build();
    }

    @GetMapping("/staffs/{staffId}/reservations")
    public ResponseEntity<List<ReservationResponse>> getReservations(@PathVariable Long staffId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(staffService.getReservations(staffId));
    }

    @ExceptionHandler(StaffIdNotFoundException.class)
    public ResponseEntity handleStaffNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .build();
    }

    @ExceptionHandler(ZoneIdInvalidException.class)
    public ResponseEntity handleZoneIdInvalidException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .build();
    }

    @ExceptionHandler(ZoneIdNotExistException.class)
    public ResponseEntity handleZoneIdNotExistException(ZoneIdNotExistException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(e.getMessage());
    }

    @ExceptionHandler(NoEnoughTimeReservationException.class)
    public ResponseEntity handleNoEnoughTimeReservationException(NoEnoughTimeReservationException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(e.getMessage());
    }

    @ExceptionHandler(TimeConflictException.class)
    public ResponseEntity handleTimeConflictException(TimeConflictException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(e.getMessage());
    }

    @ExceptionHandler(OutOfReservationTimeException.class)
    public ResponseEntity handleOutOfReservationTimeException(OutOfReservationTimeException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(e.getMessage());
    }

    @ExceptionHandler(DurationInvalidException.class)
    public ResponseEntity handleDurationInvalidException(DurationInvalidException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(e.getMessage());
    }
}
