package com.twuc.webApp.web;

import com.twuc.webApp.service.TimeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TimeController {

    private TimeService timeService;

    public TimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    @GetMapping("/api/timezones")
    public ResponseEntity getTimeZone() {
        return ResponseEntity.status(HttpStatus.OK).body(timeService.getAllZoneTime());
    }
}

