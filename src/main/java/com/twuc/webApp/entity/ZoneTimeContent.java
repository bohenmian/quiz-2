package com.twuc.webApp.entity;

public class ZoneTimeContent {

    private String zoneId;

    public ZoneTimeContent() {
    }

    public ZoneTimeContent(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
