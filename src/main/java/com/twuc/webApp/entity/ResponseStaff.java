package com.twuc.webApp.entity;

import java.time.OffsetDateTime;

public class ResponseStaff {

    private String zoneId;
    private OffsetDateTime time;

    public ResponseStaff() {
    }

    public ResponseStaff(String zoneId, OffsetDateTime time) {
        this.zoneId = zoneId;
        this.time = time;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public OffsetDateTime getTime() {
        return time;
    }

    public void setTime(OffsetDateTime time) {
        this.time = time;
    }
}
