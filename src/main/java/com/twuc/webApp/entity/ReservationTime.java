package com.twuc.webApp.entity;

public class ReservationTime {

    private ResponseClient client;

    private ResponseStaff staff;

    public ReservationTime(ResponseClient client, ResponseStaff staff) {
        this.client = client;
        this.staff = staff;
    }

    public ReservationTime() {
    }

    public ResponseClient getClient() {
        return client;
    }

    public ResponseStaff getStaff() {
        return staff;
    }
}
