package com.twuc.webApp.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.OffsetDateTime;

@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 128)
    @Column(nullable = false, length = 128)
    private String userName;

    @NotNull
    @Size(max = 64)
    @Column(nullable = false, length = 64)
    private String companyName;

    @NotNull
    @Column(nullable = false, length = 25)
    private String zoneId;

    @NotNull
    @Column
    private Instant startTime;

    @NotNull
    @Column
    private String duration;

    @ManyToOne()
    @JoinColumn(name = "staff_id")
    private Staff staff;

    public Client(String userName, String companyName, String zoneId, OffsetDateTime startTime, String duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime.toInstant();
        this.duration = duration;
    }

    public Client() {
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public void setStartTime(OffsetDateTime startTime) {
        this.startTime = startTime.toInstant();
    }
}
