package com.twuc.webApp.entity;

import java.time.OffsetDateTime;

public class ResponseClient {

    private String zoneId;

    private OffsetDateTime time;

    public ResponseClient() {
    }

    public ResponseClient(String zoneId, OffsetDateTime time) {
        this.zoneId = zoneId;
        this.time = time;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public OffsetDateTime getTime() {
        return time;
    }

    public void setTime(OffsetDateTime time) {
        this.time = time;
    }
}
