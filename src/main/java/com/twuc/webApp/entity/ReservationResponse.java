package com.twuc.webApp.entity;

import java.time.OffsetDateTime;
import java.time.ZoneId;

public class ReservationResponse {

    private String userName;
    private String companyName;
    private String duration;
    private ReservationTime startTime;

    public ReservationResponse(Client client) {
        this.userName = client.getUserName();
        this.companyName = client.getCompanyName();
        this.duration = client.getDuration();
        //TODO： 有的代码审查工具会检测单行代码字符数，所以需要换行
        ResponseClient responseClient = new ResponseClient(client.getZoneId(), OffsetDateTime.ofInstant(client.getStartTime(), ZoneId.of(client.getZoneId())));
        ResponseStaff responseStaff = new ResponseStaff(client.getStaff().getZoneId(), OffsetDateTime.ofInstant(client.getStartTime(), ZoneId.of(client.getStaff().getZoneId())));
        startTime = new ReservationTime(responseClient, responseStaff);
    }

    public ReservationResponse() {
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    public ReservationTime getStartTime() {
        return startTime;
    }
}
