package com.twuc.webApp.service.impl;

import com.twuc.webApp.entity.Client;
import com.twuc.webApp.entity.ReservationResponse;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.exception.*;
import com.twuc.webApp.repository.ClientRepository;
import com.twuc.webApp.repository.StaffRepository;
import com.twuc.webApp.service.ClientService;
import com.twuc.webApp.service.StaffService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;
    private StaffRepository staffRepository;
    private StaffService staffService;

    public ClientServiceImpl(ClientRepository clientRepository, StaffRepository staffRepository, StaffService staffService) {
        this.clientRepository = clientRepository;
        this.staffRepository = staffRepository;
        this.staffService = staffService;
    }


    @Override
    @Transactional
    public Long orderStaff(Long staffId, Client client) {
        Staff staff = staffRepository.findById(staffId).orElseThrow(StaffIdNotFoundException::new);
        isZoneIdInvalid(staff);
        isReservationTimeIllegal(client, staff);
        isTimeConflict(staffId, client, staff);
        staff.getClients().add(client);
        client.setStaff(staff);
        Client saveClient = clientRepository.save(client);
        return saveClient.getId();
    }

    //TODO: bad naming
    private void isZoneIdInvalid(Staff staff) {
        if (staff.getZoneId() == null) {
            throw new ZoneIdNotExistException("{firstName lastName} is not qualified.");
        }
    }

    private void isTimeConflict(Long staffId, Client client, Staff staff) {
        List<ReservationResponse> reservations = staffService.getReservations(staffId);
        OffsetDateTime clientOffsetStartDateTime = OffsetDateTime.ofInstant(client.getStartTime(), ZoneId.of(staff.getZoneId()));
        //TODO: duration的校验？PT{number}H??
        if (client.getDuration().length() != 4) {
            throw new DurationInvalidException("The application duration should be within 1-3 hours.");
        }

        long hours = Duration.parse(client.getDuration()).toHours();
        if (hours < 1 ||
                hours > 3) {
            throw new DurationInvalidException("The application duration should be within 1-3 hours.");
        }

        OffsetDateTime clientOffsetEndDateTime = clientOffsetStartDateTime.plusHours(hours);
        reservations.forEach(reservationResponse -> {
            OffsetDateTime staffOffsetStartDateTime = reservationResponse.getStartTime().getStaff().getTime();
            OffsetDateTime staffOffsetEndDateTime = staffOffsetStartDateTime.plusHours(Duration.parse(reservationResponse.getDuration()).toHours());
            //TODO: 抽个方法更表意
            if (!(clientOffsetStartDateTime.isBefore(staffOffsetEndDateTime) && clientOffsetEndDateTime.isBefore(staffOffsetEndDateTime) || clientOffsetStartDateTime.isAfter(staffOffsetEndDateTime))) {
                throw new TimeConflictException("The application is conflict with existing application.");
            }
        });
    }

    private void isReservationTimeIllegal(Client client, Staff staff) {
        OffsetDateTime staffOffsetDateTime = OffsetDateTime.ofInstant(client.getStartTime(), ZoneId.of(staff.getZoneId()));
        OffsetDateTime nowOffsetDateTime = OffsetDateTime.ofInstant(Instant.now(), ZoneId.of(client.getZoneId()));
        if (Duration.between(nowOffsetDateTime, staffOffsetDateTime).toHours() <= 48) {
            throw new NoEnoughTimeReservationException("Invalid time: the application is too close from now. The interval should be greater than 48 hours.");
        }
        if (staffOffsetDateTime.getHour() < 9 || staffOffsetDateTime.getHour() > 17) {
            throw new OutOfReservationTimeException("You know, our staff has their own life.");
        }
    }

}
