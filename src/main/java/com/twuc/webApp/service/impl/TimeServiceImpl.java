package com.twuc.webApp.service.impl;

import com.twuc.webApp.service.TimeService;
import org.springframework.stereotype.Service;

import java.time.zone.ZoneRulesProvider;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TimeServiceImpl implements TimeService {


    @Override
    public Set<String> getAllZoneTime() {
        return ZoneRulesProvider.getAvailableZoneIds().stream().sorted(String::compareTo).collect(Collectors.toCollection(LinkedHashSet::new));
    }

}
