package com.twuc.webApp.service.impl;

import com.twuc.webApp.entity.ReservationResponse;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneTimeContent;
import com.twuc.webApp.exception.StaffIdNotFoundException;
import com.twuc.webApp.exception.ZoneIdInvalidException;
import com.twuc.webApp.repository.StaffRepository;
import com.twuc.webApp.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class StaffServiceImpl implements StaffService {

    //TODO: unused field
    //TODO: field injection is not recommended
    @Autowired
    private EntityManager entityManager;

    private StaffRepository staffRepository;

    public StaffServiceImpl(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @Override
    public Long createStaff(Staff staff) {
        Staff saveStaff = staffRepository.save(staff);
        return saveStaff.getId();
    }

    @Override
    public Staff getStaff(Long staffId) {
        return staffRepository.findById(staffId).orElseThrow(StaffIdNotFoundException::new);
    }

    @Override
    public List<Staff> getAllStaff() {
        return staffRepository.findAll();
    }

    @Override
    public void updateStaff(Long staffId, ZoneTimeContent content) {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        if (!availableZoneIds.contains(content.getZoneId())) {
            throw new ZoneIdInvalidException();
        }
        Staff savedStaff = staffRepository.findById(staffId).orElseThrow(StaffIdNotFoundException::new);
        savedStaff.setZoneId(content.getZoneId());
        staffRepository.save(savedStaff);
    }

    @Override
    public List<ReservationResponse> getReservations(Long staffId) {
        Staff staff = staffRepository.findById(staffId).orElseThrow(StaffIdNotFoundException::new);
        List<ReservationResponse> responseList = new ArrayList<>();
        staff.getClients().forEach(client -> {
            ReservationResponse reservationResponse = new ReservationResponse(client);
            responseList.add(reservationResponse);
        });
        return responseList;
    }

}
