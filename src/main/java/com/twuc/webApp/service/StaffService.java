package com.twuc.webApp.service;

import com.twuc.webApp.entity.ReservationResponse;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.ZoneTimeContent;

import java.util.List;

public interface StaffService {

    Long createStaff(Staff staff);

    Staff getStaff(Long staffId);

    List<Staff> getAllStaff();

    void updateStaff(Long staffId, ZoneTimeContent content);

    List<ReservationResponse> getReservations(Long staffId);
}
