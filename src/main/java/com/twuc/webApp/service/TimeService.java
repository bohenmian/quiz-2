package com.twuc.webApp.service;

import java.util.Set;

public interface TimeService {

    Set<String> getAllZoneTime();
}
