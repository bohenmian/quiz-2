package com.twuc.webApp.service;

import com.twuc.webApp.entity.Client;

public interface ClientService {

    Long orderStaff(Long staffId, Client client);

}
